import scrapy
import math

class InternshalabotSpider(scrapy.Spider):
    name = 'internshalabot'
    allowed_domains = ['internshala.com']
    start_urls = ['https://internshala.com/internships/']

    custom_settings = {
        "DOWNLOAD_DELAY": 3,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 2,
        "FEED_URI" : "output.csv",
        "FEED_FORMAT" : 'csv',
    }

    def parse(self, response):
        ele =  response.xpath("//div[@class='heading_4_5 profile']/a/text()").extract()
        pages = math.ceil(500/len(ele))
        for i in range(1, pages+1):
            new_url = f'https://internshala.com/internships/page-{i}'
            yield scrapy.Request(new_url, callback=self.parse_job_urls)


    def parse_job_urls(self, response):
        links =  response.xpath("//div[@class='heading_4_5 profile']/a/@href").extract()
        base_url = 'https://internshala.com'
        for link in links:
            url = base_url+link
            yield scrapy.Request(url, callback=self.parse_job_details)


    def parse_job_details(self, response):
        job_title =  response.xpath("//span[@class='profile_on_detail_page']/text()").extract_first()
        url =  response.request.url
        company_name =  response.xpath("//div[@class='heading_6 company_name']/a/text()").extract_first().strip()
        start_time = response.xpath("//div[@id='start-date-first']/span[2]/text()").extract_first()
        last_date_to_apply = response.xpath("//div[contains(@class,'apply_by')]/div[@class='item_body']/text()").extract_first()
        duration = response.xpath("//div[@class='internship_other_details_container']/div[1]/div[2]/div[@class='item_body']/text()").extract_first().strip()
        num_of_openings = response.xpath("//div[@class='internship_details']/div[@class='text-container'][2]/text()").extract_first().strip()
        location = response.xpath("//div[@id='location_names']/span/a[@class='location_link']/text()").extract_first()
        application_msg = response.xpath("//div[@class='applications_message']/text()").extract_first().strip()
        company_website =  response.xpath("//div[@class='internship_details']/div[contains(@class,'website_link')]/a/@href").extract_first()
        stipend = response.xpath("//span[@class='stipend']/text()").extract_first().strip()
        job_type = response.xpath("//div[@class='tags_container']/div[contains(@class,'label_container_desktop')]/text()").extract_first().strip()

        scraped_info = {
            'job_title':job_title,
            'url':url,
            'company name':company_name,
            'start_time':start_time,
            'last_date_to_apply':last_date_to_apply,
            'duration':duration,
            'No of Openings': num_of_openings,
            'location':location,
            'stipend':stipend,
            'job_type':job_type,
            'application_message':application_msg,
            'company_website':company_website,
            
        }

        yield scraped_info